CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Implementation
 * Implementation with Invoice Receipt

INTRODUCTION
------------

Current Maintainer: Sebastian Schneider <sebastian@tsolutions.cl>

Commerce Webpay REQUIRES Commerce and the webpay kit from transbank.cl
We also recommend to install the Invoice Receipt module
to show a printable version and to send a html mail.

commerce_webpay module was build upon the commerce_paypay module.
Allot of code came as well from our own uc_webpay module implemented
by Ingo and Jaime Morales <jaime@jamoralesr.com>.
Thanks to all of them.


IMPLEMENTATION
-------------------

a)	Yo need an own ID of Commerce at Transbank.cl
	(in certification-mode there is a ID for development placed as default in this module)
b)	Please contact www.transbank.cl to get the last release of the kit.
	There are diferent kits for each server-type available.
c)	place a cgi-bin folder for each currency inside your drupal instalation
	(there are default values in the configuration form but security is on your side
	when you change de location of this folder)
d)	There are some keys you need to change.
	This is the first complicated point where you might have a good contact to transbank.cl
e)	Change your tbk_config.dat in the datos folder in your cgi-bin

    Here an example in chilean pesos for testing:

    IDCOMERCIO     = 597026007976
    MEDCOM         = 2
    TBK_KEY_ID     = 101
    PARAMVERIFCOM  = 1
    URLCGICOM      = http://yourdomain.com/cgi-bin-clp/tbk_bp_resultado.cgi
    SERVERCOM      = 173.236.246.19
    PORTCOM        = 80
    WHITELISTCOM   = ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789./:=&?_-
    HOST           = 173.236.246.19
    WPORT          = 80
    URLCGITRA      = /filtroUnificado/bp_revision.cgi
    URLCGIMEDTRA   = /filtroUnificado/bp_validacion.cgi
    SERVERTRA      = https://certificacion.webpay.cl
    PORTTRA        = 6443
    PREFIJO_CONF_TR   = HTML_
    HTML_TR_NORMAL  =  http://yourdomain.com/commerce_webpay/clp

    The HTML_TR_NORMAL is a static url managed by drupal,
    the last argument is the currency-code (clp or usd)

f) change the .htaccess of drupal instalation

    change:
    RewriteRule ^ index.php [L]

    with:
    #RewriteRule ^ index.php [L]
    RewriteRule ^(.*)$ index.php?q=$1 [L,QSA]

    thanks to http://www.exp-media.com/drupal7-htaccess-subfolder-tip

  maybe it is not enove but this is sandbox code
	<IfModule mod_security.c>
	    SecFilterSelective REQUEST_URI "^/commerce-webpay/clp$" "allow,pass"
	    SecFilterSelective REQUEST_URI "^/commerce-webpay/usd$" "allow,pass"
	</IfModule>

g)	Enable the module
h)	go to the payment-method menu in commerce to set values like they are in the tpk_config.dat
	(inside the payment process is a validation where you can check if tbk_config.dat
	and module configuration are placed well)
i)	enable the payment-method
j)	if you are updating rules, commerce or commerce_webpay you need to reconfirm rules settings
	(if not tests of your store and module security made by Transbank will fail)
k)	inform Transbank that they have to use with each test a new url of success and failure given to the redirect_id
	system implemented by commerce (we had lot of problems caused by misunderstandings)
l)	change the line 1182 of commerce/includes/commerce.currency.inc set "'symbol' => '$'," to "'symbol' => 'US$',"
	this is requiered by Transbank
m)	from here we need to stop explaining. there are a lot of things which might get wrong
	and once again you are in the hands of transbank.cl
	(there is a manual for developers and integration you might study it)


IMPLEMENTATION WITH INVOICE RECEIPT
-----------------------------------

To get the transaction information into the template you need to patch some code:

commerce_invoice_receipt.module

add behinde line 227:

if (isset($build['commerce_order'][$order->order_number]['commerce_webpay_order'])) {
  $info['order_transaction'] = $build['commerce_order'][$order->order_number]['commerce_webpay_order']['#markup'];
  }

theme/commerce-order-invoice-view.tpl.php

add behinde line 71:

<tr>
  <td>
	<table class="products" width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: verdana, arial, helvetica; font-size: 11px;">
	  <tbody>
		<tr>
		  <td class="line-items"><?php print $info['order_transaction']; ?></td>
		</tr>
	  </tbody>
	</table>
  </td>
</tr>
